import os
import dialogflow_v2 as dialogflow

if __name__ == '__main__':

    os.environ[
        "GOOGLE_APPLICATION_CREDENTIALS"] = r"C:\Users\Julian\Desktop\Python\BA\dialogflowcredentialsadmin.json"
    project_id = "testagent-ltcd"
    intents_client = dialogflow.IntentsClient()

    parent = intents_client.project_agent_path(project_id)

    intents = intents_client.list_intents(parent)

    intents_client.batch_delete_intents(parent, intents)

    print("Successfully deleted intents!")
