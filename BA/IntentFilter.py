import csv

if __name__ == '__main__':
    dataSet = list(csv.reader(open("atis_intents_old.csv")))

    dataDictionary = dict()
    intents_to_filter = []
    trainData = [[]]
    remaining_intents = ["atis_flight", "atis_flight_time", "atis_airfare", "atis_aircraft", "atis_ground_service", "atis_airline", "atis_abbreviation", "atis_quantity", "Fallback"]

    for data in dataSet:
        intent = data[0]
        training_phrase = data[1]

        if intent not in remaining_intents:
            continue

        if intent in dataDictionary:
            dataDictionary[intent].append(training_phrase)
        else:
            dataDictionary[intent] = []
            dataDictionary[intent].append(training_phrase)

    for intent in dataDictionary:
        #if len(dataDictionary[intent]) < 12:
        #    intents_to_filter.append(intent)
        if len(dataDictionary[intent]) >= 50:
            dataDictionary[intent] = dataDictionary[intent][0:50]
        else:
            continue

    for intent in intents_to_filter:
        dataDictionary.pop(intent, None)

    #Write intents to final 2d array
    for intent in dataDictionary:
        i = 0
        for phrase in dataDictionary[intent]:
            trainData.append([intent, phrase])

    with open("balanced_big" + ".csv", "w", newline="") as trainFile:
        writer1 = csv.writer(trainFile)
        writer1.writerows(trainData)
