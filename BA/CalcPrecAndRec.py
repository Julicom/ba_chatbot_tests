import pandas as pd
import numpy as np
from sklearn.metrics import precision_recall_fscore_support

if __name__ == '__main__':
    dataFrame1 = pd.read_csv("Dialogflow_Run_1_Balanced_Big_Updated.csv")
    dataFrame2 = pd.read_csv("Dialogflow_Run_2_Balanced_Big_Updated.csv")
    dataFrame3 = pd.read_csv("Dialogflow_Run_3_Balanced_Big_Updated.csv")
    dataFrame4 = pd.read_csv("Dialogflow_Run_4_Balanced_Big_Updated.csv")
    dataFrame5 = pd.read_csv("Dialogflow_Run_5_Balanced_Big_Updated.csv")

    confusionMatrix2 = np.array(pd.DataFrame.to_numpy(dataFrame2)[:, 1:])
    confusionMatrix3 = np.array(pd.DataFrame.to_numpy(dataFrame2)[:, 1:])
    confusionMatrix4 = np.array(pd.DataFrame.to_numpy(dataFrame2)[:, 1:])
    confusionMatrix5 = np.array(pd.DataFrame.to_numpy(dataFrame2)[:, 1:])

    confusionMatrixWithLabels = pd.DataFrame.to_numpy(dataFrame1)
    intentList = []
    predictedIntents = []
    actualIntents = []

    for intent in confusionMatrixWithLabels.T[0]:
        intentList.append(intent)

    confusionMatrixWithoutLabels = np.array(confusionMatrixWithLabels[:, 1:])

    confusionMatrixWithoutLabels = np.add(confusionMatrixWithoutLabels, confusionMatrix2)
    confusionMatrixWithoutLabels = np.add(confusionMatrixWithoutLabels, confusionMatrix3)
    confusionMatrixWithoutLabels = np.add(confusionMatrixWithoutLabels, confusionMatrix4)
    confusionMatrixWithoutLabels = np.add(confusionMatrixWithoutLabels, confusionMatrix5)

    for x in range(len(confusionMatrixWithoutLabels) - 1):
        for y in range(len(confusionMatrixWithoutLabels[x]) - 1):
            for z in range(confusionMatrixWithoutLabels[x][y]):
                actualIntents.append(intentList[x])
                predictedIntents.append(intentList[y])

print(confusionMatrixWithoutLabels)
print(precision_recall_fscore_support(actualIntents, predictedIntents, average='macro'))