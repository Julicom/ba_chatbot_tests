import csv
import dialogflow_v2 as df
import os
import time
import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix


if __name__ == '__main__':

    os.environ[
        "GOOGLE_APPLICATION_CREDENTIALS"] = r"C:\Users\Julian\Desktop\Python\BA\dialogflowcredentialsadmin.json"
    projectId = "testagent-ltcd"
    dataSet = list(csv.reader(open("test_set_balanced_big2.csv")))

    session_client = df.SessionsClient()
    session = session_client.session_path(projectId, "123456789")

    actualIntents = []
    detectedIntents = []
    intents = ["atis_flight", "atis_flight_time", "atis_airfare", "atis_aircraft", "atis_ground_service", "atis_airline", "atis_abbreviation", "atis_quantity", "Fallback"]
    #intents = ['atis_flight', 'atis_flight_no', 'Fallback']
    i = 1
    for data in dataSet:
        actualIntent = data[0]
        test_phrase = data[1]

        actualIntents.append(actualIntent)

        print("============================================================================")
        print("Testphrase number: ", i)
        print("Actual intent: ", actualIntent)

        text_input = df.types.TextInput(text=test_phrase, language_code="en-US")
        query_input = df.types.QueryInput(text=text_input)

        response = session_client.detect_intent(session=session, query_input=query_input)
        detectedInent = response.query_result.intent.display_name

        detectedIntents.append(detectedInent)

        print("Detected intent: ", detectedInent)

        print("============================================================================")

        time.sleep(0.35)

        i += 1

    df_cm = pd.DataFrame(confusion_matrix(actualIntents, detectedIntents, labels=intents), index=intents,
                         columns=intents)
    sn.heatmap(df_cm, annot=True)

    df_cm.to_csv('Dialogflow_Run_2_Balanced_Big_Updated.csv')

    plt.show()