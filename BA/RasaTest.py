import requests
import csv
import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

if __name__ == '__main__':

    dataSet = list(csv.reader(open("test_set_balanced_big5.csv")))

    actualIntents = []
    detectedIntents = []
    #intents = ['atis_flight', 'atis_flight_time', 'atis_airfare', 'atis_aircraft', 'atis_ground_service',
    #           'atis_airport', 'atis_airline', 'atis_distance', 'atis_abbreviation', 'atis_ground_fare',
    #           'atis_quantity', 'atis_city', 'atis_flight_no', 'atis_capacity', 'nlu_fallback']
    intents = ['atis_flight', 'atis_flight_time', 'atis_airfare', 'atis_aircraft',
               'atis_ground_service', 'atis_airline', 'atis_abbreviation', 'atis_quantity', 'nlu_fallback']
    #intents = ['atis_flight', 'atis_flight_no', 'nlu_fallback']
    i = 1

    for data in dataSet:
        testPhrase = data[1]
        actualIntent = data[0]

        actualIntents.append(actualIntent)

        print("============================================================================")
        print("Testphrase number: ", i)
        print("Actual intent: ", actualIntent)

        test = dict()
        test["text"] = testPhrase
        test["message_id"] = "123456789"
        response = requests.post("http://192.168.0.35:5005/model/parse", json=test)
        detectedIntent = response.json()['intent']['name']

        detectedIntents.append(detectedIntent)

        print("Detected intent: ", detectedIntent)

        print("============================================================================")

        i += 1

    df_cm = pd.DataFrame(confusion_matrix(actualIntents, detectedIntents, labels=intents), index=intents, columns=intents)
    sn.heatmap(df_cm, annot=True)
    plt.show()

    df_cm.to_csv('Rasa_Run_5_Balanced_Result.csv')

    print("TEST COMPLETED!")
