import csv
import itertools

import numpy as np
from sklearn.model_selection import StratifiedKFold

if __name__ == '__main__':
    dataSet = list(csv.reader(open("balanced_big.csv")))

    dataDictionary = dict()
    trainingPhrasesList = []
    intentList = []
    intents_to_remove = []

# Section: Filter bad training data
    for data in dataSet:
        intent = data[0]
        training_phrase = data[1]

        if len(training_phrase) > 255:
            continue

        if intent in dataDictionary:
            dataDictionary[intent].append(training_phrase)
        else:
            dataDictionary[intent] = []
            dataDictionary[intent].append(training_phrase)

    for key in dataDictionary:
        if len(dataDictionary[key]) < 50:
            intents_to_remove.append(key)

    for intent_to_remove in intents_to_remove:
        dataDictionary.pop(intent_to_remove, None)

    for key in dataDictionary:
        if len(dataDictionary[key]) > 50 and key != "atis_flight":
            dataDictionary[key] = itertools.islice(dataDictionary[key], 50)
        elif key == "atis_flight":
            dataDictionary[key] = itertools.islice(dataDictionary[key], 500)

# End Section: Filter bad training data

    for key in dataDictionary:
        for phrase in dataDictionary[key]:
            intentList.append(key)
            trainingPhrasesList.append(phrase)

    skfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)

    trainingPhrasesListNP = np.array(trainingPhrasesList)
    intentListNP = np.array(intentList)

    i = 1

    for train_index, test_index in skfold.split(trainingPhrasesListNP, intentListNP):
        trainingPhrasesSplit, testPhrasesSplit = trainingPhrasesListNP[train_index], trainingPhrasesListNP[test_index]
        trainingIntentsSplit, testIntentsSplit = intentListNP[train_index], intentListNP[test_index]

        trainDataPart = np.array(list(zip(trainingIntentsSplit, trainingPhrasesSplit)))
        testDataPart = np.array(list(zip(testIntentsSplit, testPhrasesSplit)))

        with open("train_set_balanced_big" + str(i) + ".csv", "w", newline="") as trainFile:
            writer1 = csv.writer(trainFile)
            writer1.writerows(trainDataPart)

        with open("test_set_balanced_big" + str(i) + ".csv", "w", newline="") as testFile:
            writer2 = csv.writer(testFile)
            writer2.writerows(testDataPart)

        i += 1

    print("Fertig!")